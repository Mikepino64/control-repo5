# Class: profile::wordpress

class profile::wordpress {
  include wordpress
  include mysql::server
  include apache
  include apache::mod::php

  package { 'php-mysqlnd':
    ensure => installed,
    notify => Service['httpd'],
  }
  file { '/etc/puppetlabs/facter/facts.d/application.txt':
    ensure => file,
    source => 'puppet:///modules/profile/wordpress-application',
    owner  => 'root',
    group  => 'root',
    mode   => '0644',
    }
}
