class profile::base {
  include ntp

if $facts['os']['family'] == 'RedHat' {
  package { ['bash-completion', 'tree']:
    ensure => installed,
    }
    }
file {['/etc/puppetlabs/facter','/etc/puppetlabs/facter/facts.d']:
  ensure => directory,
    }

}
